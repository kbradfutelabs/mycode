#!/usr/bin/python3

import requests
import datetime
import reverse_geocoder as rg

URL = "http://api.open-notify.org/iss-now.json"

def main():
    resp = requests.get(URL).json()

    # Current location of the ISS in longitute and latitude
    lon = resp["iss_position"]["longitude"]
    lat = resp["iss_position"]["latitude"]

    # Current timestamp of the ISS location coordinates
    timestamp = resp["timestamp"]
    timestamp = datetime.datetime.fromtimestamp(timestamp)
    timestamp = str(timestamp.strftime("%m-%d-%Y"))

    # Return an ordered dictionary using lat and lon variables
    result = rg.search((lat, lon))

    # Slice the object to return the city name only
    city = result[0]["name"]

    # Slice the object again to return the country
    country = result[0]["cc"]


    print("CURRENT LOCATION OF THE ISS:\n")
    print(f"""
    Timestamp:    {timestamp}
    Lon:          {lon}
    Lat:          {lat}
    City/Country: {city}, {country}
    """)

if __name__ == "__main__":
    main()
