#!/usr/bin/env python3

import netifaces

print(netifaces.interfaces())

for i in netifaces.interfaces():
    print('\n****** details of interface - ' + i + ' ******')
    try:
        print('MAC: ', end='') # This print statement will always print MAC without an end of line
        print((netifaces.ifaddresses(i)[netifaces.AF_LINK])[0]['addr']) # Prints the MAC address
        print('IP: ', end='')  # This print statement will always print IP without an end of line
        print((netifaces.ifaddresses(i)[netifaces.AF_INET])[0]['addr']) # Prints the IP address
    except:          # This is a new line
        print('Could not collect adapter information') # Print an error message

# User needs to choose which adapter to get the MAC addr from
inter = input("Which interface would you like to see the MAC address for?")

print(netifaces.ifaddresses(inter))

print("\n")
# Let's borrow a line from lab 23
print("MAC: ", end="")
print((netifaces.ifaddresses(inter)[netifaces.AF_LINK])[0]["addr"])
